import mongoose from 'mongoose';
import { getTemperature, getTemperatureByCity } from '../../src/db';
import Cities from '../../src/model';

describe('when getTemperature method is called', () => {
    let mongooseSpy;

    beforeEach(() => {
        mongoose.connection = { readyState: 0 };
        mongooseSpy = jest.spyOn(mongoose, 'connect').mockImplementation();
    });


    describe('when database connection is already established', () => {
        beforeEach(() => {
            mongoose.connection.readyState = 1;
        });

        afterEach(() => {
            mongoose.connection.readyState = 0;
        });
        it('returns temperature for all cities', async () => {
            Cities.find = jest.fn().mockResolvedValueOnce([
                {
                    name: 'Sydney',
                    temperature: '24'
                }
            ]);
            const results = await getTemperature();
            expect(mongooseSpy).not.toHaveBeenCalled();
            expect(results).toEqual({ cities: [
                {
                    name: 'Sydney',
                    temperature: '24'
                }
            ]});
        });
    });

    describe('when database connection can be established', () => {
        it('returns temperature for all cities', async () => {
            Cities.find = jest.fn().mockResolvedValueOnce([
                {
                    name: 'Sydney',
                    temperature: '24'
                }
            ]);
            mongooseSpy.mockResolvedValueOnce(true);
            const results = await getTemperature();
            expect(results).toEqual({ cities: [
                {
                    name: 'Sydney',
                    temperature: '24'
                }
            ]});
        });
    });

    describe('when databse connection cannot be established', () => {
        it('throws an error', async () => {
            mongooseSpy.mockRejectedValueOnce(new Error('Connection failed'));
            try {
                await getTemperature();
            } catch (error) {
                expect(error).toBeDefined();
                expect(error).toEqual(expect.any(Error));
            };
        });
    });
});