import express from 'express';
import cors from 'cors';
import { PORT, logger } from './config/index';
import { getTemperature } from './src/db';

const app = express();
app.use(cors());

app.get('/temperature/city',  async (req, res) => {
    const results = await getTemperature();
    return res.status(200).send(results);
});

app.listen(PORT, () => {
    logger.info(`Serving at port ${PORT}`);
});