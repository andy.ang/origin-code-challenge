import bunyan from 'bunyan';

const log = bunyan.createLogger({ name: 'weather-service' });
const port = process.env.SERVER_PORT;

export const PORT = port;
export const logger = log;