import mongoose from 'mongoose';

const citiesSchema = mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    temperature: {
        type: String
    }
}, { 
    Collection: 'Cities', 
    collation: { locale: 'en_US', strength: 2 } 
});

const Cities = mongoose.model('Cities', citiesSchema);

export default Cities;