import mongoose from 'mongoose';
import "core-js/stable";
import "regenerator-runtime/runtime";
import Cities from '../model';
import { logger } from '../../config';

const conn = mongoose.connection;

export const getTemperature = async () => {
    try {
        if (conn.readyState === 0) {
            await mongoose.connect(`mongodb://mongodb`);
            logger.info('Connecting to database');
        }

        logger.info('Querying database.');
        const cities = await Cities.find();
        return { cities }
    } catch (error) {
        logger.error(error);
        throw error;
    }
}