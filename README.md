# Origin code challenge

Dockerfiles are used for convenience and ensure of a consistent running environment.

There are multiple docker files in this project.  
`Dockerfile.api` - serves the api / backend  
`Dockerfile.client` - serves the frontend  
`Dockerfile.seed` - inserts records from `weather.json` into mongo  

The docker-compose file is used to bring up all the necessary services in the right order. 

## Getting started
The frontend is created with `create-react-app` hence it is a repository on its own. To put it in the same repository as the backend code I decided to include it as a submodule. 

After `git clone`, run `git submodule update --init --recursive` to pull in the client code. 
Do `npm install` / `yarn install` in both `/api` and `/client`. 

Finally,
```
docker-compose up
```
Visit [http://localhost:3001](http://localhost:3001) to view the webpage. 

## How to test
`cd` into `/api` or `/client` and run
```
npm test
```

For coverage:
```
npm test --coverage
```

## Troubleshooting
Make sure all the dependencies are installed in both `api` and `client`.
The api is served at [localhost:8081/temperature/city](localhost:8081/temperature/city). Hit this endpoint to ensure data is returned.